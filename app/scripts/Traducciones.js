window.voxcivica = window.voxcivica || {};

(function (voxcivica, undefined) {
  var SPANISH, ONE_WEEK, TRANSLATION_FILES_PATH;
  var englishOrdinalFor, options, changeLanguage;

  SPANISH = 'es';
  ONE_WEEK = 86400000;
  TRANSLATION_FILES_PATH = '/i18n/__ns__/__lng__.json';

  options = {
    lng:SPANISH,
    fallbackLng:SPANISH,
    resGetPath:TRANSLATION_FILES_PATH,
    debug:true,
    useLocalStorage:false,
    localStorageExpirationTime:ONE_WEEK,
    ns:{
      namespaces:['nav', 'tagline', 'home', 'que-es',
        'equipo', 'acciones', 'asociarse', 'contact',
        'footer', 'creditos'],
      defaultNs:'nav'
    }
  };

  changeLanguage = function (idioma) {
    $.i18n.setLng(idioma, function () {
      $(document).i18n();
    });
  };

  voxcivica.Traducciones = function (selector) {
    return {
      init:function () {
        i18n.init(options, function () {
          changeLanguage(SPANISH);
        });
        $(selector + ' a').on('click', function (e) {
          e.preventDefault();
          changeLanguage($(this).data().language);
          $(this).parents('ul').find('li').removeClass('active');
          $(this).parent().addClass('active');
        });
      }
    };
  };
}(window.voxcivica));