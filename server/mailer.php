<?php
require_once('swift_required.php');

header('Access-Control-Allow-Origin: *');  

$subject = "Web VoxCivica - Formulario de contacto";
$nombre = $_POST['nombre'];
$email = $_POST['email'];
$mensaje = $_POST['mensaje'];

if ($nombre == '' || $email == '' || $mensaje == '')
	die(json_encode(array('result' => 'ko', 'error' => 'wrong payload')));


Swift::init(function () {
		Swift_DependencyContainer::getInstance()
		->register('mime.qpcontentencoder')
		->asAliasOf('mime.nativeqpcontentencoder');
		});

// Create the message
$message = Swift_Message::newInstance()
	->setSubject($subject)
	->setFrom(array('hola@voxcivica.org' => 'Hola VoxCivica'))
	->setTo(array('guille@ggalmazor.com'))
	->setBody("{$nombre}\n{$email}\n\n{$mensaje}");

$transport = Swift_SmtpTransport::newInstance('smtp.gmail.com', 465, 'ssl')
	->setUsername('hola@voxcivica.org')
	->setPassword('V0xC1v1c4');

$mailer = Swift_Mailer::newInstance($transport);

if ($mailer->send($message))
	die(json_encode(array('result'=>'ok')));
die(json_encode(array('result'=>'ko','error'=>'send error')));

?>
